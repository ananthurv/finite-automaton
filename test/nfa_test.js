var assert = require('chai').assert;
var nfa_generator = require('../src/nfa.js').nfa;

describe("NFA Generator", function(){
    describe("NFA for languages without epsilon", function(){
        var language = {
            states: ["q1", "q2", "q3", "q4"],
            alphabets: ["1", "0"],
            transition_func: {
                "q1": {"1" : ["q1", "q3"], "0" : ["q1", "q2"]},
                "q2": {"0" : ["q4"]},
                "q3": {"1" : ["q4"]},
                "q4": {"0": ["q4"], "1": ["q4"]}
            },
            initial_state: "q1",
            final_states: ["q4"]
        }
    
        var nfa_for_strings_containing_subset_00_or_11 = nfa_generator(language);
        it("should return true for 1000101", function(){
            assert.isTrue(nfa_for_strings_containing_subset_00_or_11("1000101"));
        });
        it("should return true for 101", function(){
            assert.isFalse(nfa_for_strings_containing_subset_00_or_11("101"));
        });
    });

    describe("NFA for languages with epsilon", function(){
        var language = {
            "states" : ["q1", "q2", "q3", "q4", "q5", "q6"],
            "alphabets" : ["1", "0", 'e'],
            "transition_func" : {
                "q1": {'e': ["q2","q3"]},
                "q2": {"1": ["q2"], "0":["q4"]},
                "q3": {"0": ["q3"], "1":["q5"]},
                "q4": {"0": ["q2"], "1":["q2"]},
                "q5": {"0": ["q5"],"1": ["q6"]},
                "q6": {"0": ["q6"]}
            },
            "initial_state" : "q1" ,
            "final_states" : ["q2", "q6"]
        };
    
        var nfa_for_strings_contains_exactly_two_1s_or_even_0s = nfa_generator(language);
        it("should return true for 11", function(){
            assert.isTrue(nfa_for_strings_contains_exactly_two_1s_or_even_0s("11"));
        });
        it("should return true for 00", function(){
            assert.isTrue(nfa_for_strings_contains_exactly_two_1s_or_even_0s("00"));
        });
        it("should return true for 1001", function(){
            assert.isTrue(nfa_for_strings_contains_exactly_two_1s_or_even_0s("1001"));
        });
    });

    describe('Language w | w is the string ending with 0', function () {
        var language = {
            "states": ["q1", "q2", "q3", "q4"],
            "alphabets": ["0", "1"],
            "transition_func": {
                "q1": {'e': ["q2"]},
                "q2": {'e': ["q3"]},
                "q3": {"0": ["q3", "q4"], "1": ["q3"]},
                "q4": {}
            },
            "initial_state": "q1",
            "final_states": ["q4"]

        };
        var nfa = nfa_generator(language);
        it("should pass for 00", function () {
            assert.ok(nfa("00"));
        });
        it("should pass for 01110", function () {
            assert.ok(nfa("01110"));
        });
        it("should fail for 11", function () {
            assert.notOk(nfa("11"));
        });
    });
});