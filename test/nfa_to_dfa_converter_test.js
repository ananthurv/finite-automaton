var assert = require('chai').assert;
var combinations = require('../src/nfa_to_dfa_converter').combinations;

describe('NFA TO DFA Converter', function(){
    
    describe('Combinations should give all possible combinations of an array', function () {
        it('Array of length 3 should give me 7 combinations excluding empty',function () {
            assert.equal(combinations([1,2,3]).length, 7);
        });
    });
});