var state_mapper =function(other_states, result, first_state) {
    return other_states.map(function (each_rest) {
        result.push(first_state.concat(each_rest));
        return first_state.concat(each_rest);
    });
};

var get_combination = function (first_state, other_states, result) {
    var combinations = state_mapper(other_states, result, first_state);
    combinations.slice(0,-1).forEach(function(combination, index){
        get_combination(combination, other_states.slice(index+1), result);
    });
    return;
};

var combinations = function (set) {
    var result = [];
    set.map(function(state, index){
        return get_combination(set.slice(index,index+1),set.slice(index+1),result);
    });
    return set.concat(result);
};

exports.combinations = combinations;