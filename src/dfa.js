var _ = require('lodash');

function reducer(characters, language) {
    return characters.reduce(function (state, alphabet) {
        return language.transition_func[state][alphabet];
    }, language.initial_state);
}

exports.dfa = function(language){
    return function(string){
        var characters = string.split("");
        var state = reducer(characters, language);
        return _.includes(language.final_states,state);
    }
};