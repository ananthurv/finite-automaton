const _ = require('lodash');
const epsilon = 'e';

var is_subset_of = function (subSet, superSet) {
    return subSet.every(function (element) {
        return _.includes(superSet, element);
    })
};
var nfa = function(language){

    var get_epsilon_transitions = function(states){
        var epsilon_transitions = _.flatten(states.map(function (state) {
            return language.transition_func[state] && language.transition_func[state][epsilon] || [];
        }));
        if (is_subset_of(epsilon_transitions,states)) {
            return states;}
        var all_trasitions = _.union(states,epsilon_transitions);
        return get_epsilon_transitions(all_trasitions);

    };

    var state_reducer = function(states, alphabet){
        var get_transitions = function(state) {
            return language.transition_func[state] && language.transition_func[state][alphabet] || [];
        };

        var transitions = _.flatten(states.map(get_transitions));
        return get_epsilon_transitions(transitions);
    };

    return function(input){
        var characters = input.split("");

        var epsilon_transitions = get_epsilon_transitions([language.initial_state]);
        var all_transitions = characters.reduce(state_reducer,epsilon_transitions);
        return _.intersection(all_transitions,language.final_states).length > 0;
    };
};
exports.nfa = nfa;